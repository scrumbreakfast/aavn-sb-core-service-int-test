package com.axonactive.scrumbreakfast.demo.inttest;

/**
 * Created by vhphat on 9/10/2015.
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

public class HttpSupport {
    private static Logger logger = LoggerFactory.getLogger(HttpSupport.class);
    private ObjectMapper mapper = new ObjectMapper();

    public HttpResult makeHttpGetRequest(String url, Map<String, Object> queryStringParams){
        // add query parameters.
        StringBuilder stringBuilder = new StringBuilder(url);
        if (queryStringParams != null) {
            stringBuilder.append('?');

            for (Map.Entry<String, Object> entry: queryStringParams.entrySet()) {
                stringBuilder.append(String.format("%s=%s", entry.getKey(), entry.getValue()));
                stringBuilder.append('&');
            }
            url = stringBuilder.toString();
            url = url.substring(0, url.length()-1);
        }

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response = null;
        HttpResult httpResult = new HttpResult(HttpStatus.SC_NOT_FOUND);
        try {
            response = httpclient.execute(httpGet);
            httpResult.setStatusCode(response.getStatusLine().getStatusCode());
            httpResult.setBody(mapper.readTree(response.getEntity().getContent()));
            response.close();
        } catch (IOException e) {
            logger.error("Exception executing http request", e);
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    logger.error("Exception closing response", e);
                }
            }

        }
        return httpResult;
    }
}
