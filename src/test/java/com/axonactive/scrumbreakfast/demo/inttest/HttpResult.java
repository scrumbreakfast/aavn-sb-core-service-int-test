package com.axonactive.scrumbreakfast.demo.inttest;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Created by vhphat on 9/10/2015.
 */
public class HttpResult {
    private int statusCode;
    private JsonNode body;

    public HttpResult(int statusCode) {
        this.statusCode = statusCode;
    }

    public JsonNode getBody() {
        return body;
    }

    public void setBody(JsonNode body) {
        this.body = body;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
