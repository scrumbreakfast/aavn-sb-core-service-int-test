package com.axonactive.scrumbreakfast.demo.inttest.controller;

import com.axonactive.scrumbreakfast.demo.enums.MathOperation;
import com.axonactive.scrumbreakfast.demo.inttest.HttpResult;
import com.axonactive.scrumbreakfast.demo.inttest.HttpSupport;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;

/**
 * Created by vhphat on 9/10/2015.
 */
public class CalculatorControllerIntTest extends HttpSupport {
    //private String rootUrl = "http://192.168.77.76:8080/core_service/calculator/";
    //private String rootUrl = "http://localhost:8080/calculator/";
    private String rootUrl = System.getProperty("CORE_SERVICE_URL");
    @Test
    public void test_missing_params() {
        // Given

        // When
        HttpResult result = makeHttpGetRequest(rootUrl + MathOperation.add, null);

        // Then
        assertEquals(200, result.getStatusCode());
        assertEquals("false", result.getBody().get("success").asText());
        assertEquals("Required double parameter 'num1' is not present", result.getBody().get("error").textValue());
    }

    @Test
    public void test_wrong_params_data_type() {
        // Given
        Map<String, Object> queries = new HashMap<String, Object>() {{
            put("num1", 2);
            put("num2", "an_incorrect_params");
        }};

        // When
        HttpResult result = makeHttpGetRequest(rootUrl + MathOperation.add, queries);

        // Then
        assertEquals(200, result.getStatusCode());
        assertEquals("false", result.getBody().get("success").asText());
        assertEquals("Failed to convert value of type 'java.lang.String' to required type 'double'; nested exception is java.lang.NumberFormatException: For input string: \"an_incorrect_params\"",
                result.getBody().get("error").textValue());
    }

    @Test
    public void test_add_ok() {
        // Given
        Map<String, Object> queries = new HashMap<String, Object>() {{
            put("num1", 2);
            put("num2", 3);
        }};

        // When
        HttpResult result = makeHttpGetRequest(rootUrl + MathOperation.add, queries);

        // Then
        assertEquals(200, result.getStatusCode());
        assertEquals(5.0, result.getBody().get("result").asDouble());
    }

    @Test
    public void test_minus_ok() {
        // Given
        Map<String, Object> queries = new HashMap<String, Object>() {{
            put("num1", 2);
            put("num2", 3);
        }};

        // When
        HttpResult result = makeHttpGetRequest(rootUrl + MathOperation.minus, queries);

        // Then
        assertEquals(200, result.getStatusCode());
        assertEquals(-1.0, result.getBody().get("result").asDouble());
    }

    @Test
    public void test_multiply_ok() {
        // Given
        Map<String, Object> queries = new HashMap<String, Object>() {{
            put("num1", 2.2);
            put("num2", 3.3);
        }};

        // When
        HttpResult result = makeHttpGetRequest(rootUrl + MathOperation.multiply, queries);

        // Then
        assertEquals(200, result.getStatusCode());
        assertEquals(7.26, result.getBody().get("result").asDouble(), 0.001);
    }

    @Test
    public void test_divide_ok() {
        // Given
        Map<String, Object> queries = new HashMap<String, Object>() {{
            put("num1", 3.56);
            put("num2", 2);
        }};

        // When
        HttpResult result = makeHttpGetRequest(rootUrl + MathOperation.divide, queries);

        // Then
        assertEquals(200, result.getStatusCode());
        assertEquals(1.78, result.getBody().get("result").asDouble(), 0.001);
    }

    @Test
    public void test_divide_with_scale() {
        // Given
        Map<String, Object> queries = new HashMap<String, Object>() {{
            put("num1", 3.56456);
            put("num2", 2.12313);
            put("scale", 3);
        }};

        // When
        HttpResult result = makeHttpGetRequest(rootUrl + MathOperation.divide, queries);

        // Then
        assertEquals(200, result.getStatusCode());
        assertEquals(1.679, result.getBody().get("result").asDouble(), 0.001);
    }

    @Test
    public void test_divison_by_zero() {
        // Given
        Map<String, Object> queries = new HashMap<String, Object>() {{
            put("num1", 2);
            put("num2", 0);
        }};

        // When
        HttpResult result = makeHttpGetRequest(rootUrl + MathOperation.divide, queries);

        // Then
        assertEquals(200, result.getStatusCode());
        assertEquals("false", result.getBody().get("success").asText());
        assertEquals("Division by zero error.", result.getBody().get("error").asText());
    }
}
